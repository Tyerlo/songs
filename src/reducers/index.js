import { combineReducers } from "redux";
const songsReducer = () => {
  return [
    { title: "Smells", duration: "5:00" },
    { title: "Despacito", duration: "3:00" },
    { title: "Barbie girl", duration: "2:50" }
  ];
};

const selectSongReducer = (selectedSong = null, action) => {
  if (action.type === "SONG_SELECTED") {
    return action.payload;
  }
  return selectedSong;
};

export default combineReducers({
  songs: songsReducer,
  selectedSong: selectSongReducer
});
